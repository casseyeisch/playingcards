//Cassey Eisch's forked repository for part 2

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank{TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE};

enum Suit
{
	DIAMONDS,
	CLUBS,
	SPADES,
	HEARTS
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card)
{
	if (card.rank == TWO) cout << "The Two of ";
	if (card.rank == THREE) cout << "The Three of ";
	if (card.rank == FOUR) cout << "The Four of ";
	if (card.rank == FIVE) cout << "The Five of ";
	if (card.rank == SIX) cout << "The Six of ";
	if (card.rank == SEVEN) cout << "The Seven of ";
	if (card.rank == EIGHT) cout << "The Eight of ";
	if (card.rank == NINE) cout << "The Nine of ";
	if (card.rank == TEN) cout << "The Ten of ";
	if (card.rank == JACK) cout << "The Jack of ";
	if (card.rank == QUEEN) cout << "The Queen of ";
	if (card.rank == KING) cout << "The King of ";
	if (card.rank == ACE) cout << "The Ace of ";
	//cout << card.rank << " " << card.suit << "\n"; -- This would just put out a number, not the word value
	if (card.suit == SPADES) cout << "Spades. \n";
	if (card.suit == CLUBS) cout << "Clubs. \n";
	if (card.suit == HEARTS) cout << "Hearts. \n";
	if (card.suit == DIAMONDS) cout << "Diamonds. \n";

}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank >= card2.rank)// I used >= because otherwise if they are the same it wouldn't send back a card
	{
		return card1;
	}
	else
	{
		return card2;
	}
}

int main()
{//I added a lot of options to show different examples and comparisons
	Card highCard;
	Card c1;
	Card c2;
	Card c3;
	Card c4;
	Card c5;
	Card c6;
	Card c7;
	Card c8;
	c1.rank = NINE;
	c1.suit = DIAMONDS;
	c2.rank = EIGHT;
	c2.suit = SPADES;
	c3.rank = ACE;
	c3.suit = HEARTS;
	c4.rank = JACK;
	c4.suit = CLUBS;
	c5.rank = TWO;
	c5.suit = SPADES;
	c6.rank = KING;
	c6.suit = CLUBS;
	c7.rank = FIVE;
	c7.suit = DIAMONDS;
	c8.rank = EIGHT;
	c8.suit = HEARTS;
	highCard = HighCard(c1, c2);
	PrintCard(highCard);

	highCard = HighCard(c4, c8);
	PrintCard(highCard);

	highCard = HighCard(c3, c5);
	PrintCard(highCard);

	highCard = HighCard(c2, c7);
	PrintCard(highCard);

	highCard = HighCard(c6, c4);
	PrintCard(highCard);

	highCard = HighCard(c8, c2); //either could print, but the 1st one(c8) prints because they have the same value
	PrintCard(highCard);

	_getch();
	return 0;
}